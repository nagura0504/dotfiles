" Vi 互換なし
if &compatible
    set nocompatible               " Be iMproved
endif

" ファイルタイプに応じた自動インデント
filetype on
filetype plugin indent on

" Character encoding Setting  ref="http://www.tooyama.org/vim-2.html"
set encoding=utf-8
set fileencodings=iso-2022-jp,cp932,sjis,euc-jp,utf-8
scriptencoding utf-8

"syntax on"
syntax on

" タブが対応する空白の数
set tabstop=4
" タブやバックスペースの使用等の編集操作をするときに、タブが対応する空白の数
set softtabstop=4
" インデントの各段階に使われる空白の数
set shiftwidth=4
" タブを挿入するとき、代わりに空白を使う
set expandtab

"行番号を表示"
set number
"カーソルの位置を右下に表示"
set ruler
"カーソルのある行にライン"
"set cursorline
"最後の行に常にステータスを表示"
set laststatus=2
"下のメッセージ二行まで"
set cmdheight=2
"編集中のファイル名非表示"
set notitle
"不可視文字を見えるようにする"
set list
set listchars=tab:▸\ ,eol:↲,extends:❯,precedes:❮
"ビープ音完全に消す"
set visualbell t_vb=
set noerrorbells
"高度な自動インデント"
set smartindent
"検索が先頭にループ"
set wrapscan
"検索時に大文字小文字の区別をつけない"
set ignorecase
"カーソルの先の視界を確保"
set scrolloff=12
"カーソルが端のとき行をまたぐ"
set whichwrap=b,s,h,l,<,>,[,]
"デフォルトで置換の時その行のすべてのマッチを置き換える"
set gdefault
"保存されてないときは終了時に保存確認"
set confirm
"保存されてないファイルがあるときでも他のファイルを開ける"
set hidden
"外部でファイルが変更されたときは読み直す"
set autoread
"ファイル保存時にバックアップを作らない"
set nobackup
"スワップファイルを作らない"
set noswapfile

"自動で括弧補完"
inoremap { {}<LEFT>
inoremap [ []<LEFT>
inoremap ( ()<LEFT>
inoremap " ""<LEFT>
inoremap ' ''<LEFT>
";,:をイレカエル"
nnoremap ; :
nnoremap : ;
"exモード無視"
nnoremap Q <nop>
"ノーマルモードを維持したままOで下に空行を作れる"
nnoremap <silent> O :<C-u>call append(expand('.'), '')<Cr>j


"colorschemeより前に記述.
autocmd ColorScheme * highlight Normal ctermbg=none
autocmd ColorScheme * highlight LineNr ctermbg=none
autocmd Colorscheme * highlight Visual ctermbg=8colorscheme
"カラースキーム"
set background=dark
colorscheme hybrid 


