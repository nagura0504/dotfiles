if [ -f /home/default/etc/zshrc ] ; then
    . /home/default/etc/zshrc
fi

# コマンド(含内部コマンド)入力中にマニュアルを表示できる：M-h
[ -n "`alias run-help`" ] && unalias run-help
autoload -U run-help

umask 022

# alias
if [ -f ~/.zaliases ] ; then
    source ~/.zaliases
fi

### prompt
setopt prompt_subst
PROMPTCOLOR=31
ESC=$(print -Pn '\e')
BELL=$(print -Pn '\a')

# タイトルの設定
case $TERM in
dumb|emacs|sun) unset XTITLE ;;
vt100|[Exk]term*|rxvt|cygwin)
	XTITLE=$ESC']2;$TERM %n@%m:%~'$BELL$ESC']1;%m:%.'$BELL
;;
screen)	XTITLE=$ESC'k%n@%m:%.'$ESC\\ ;;
esac

# ターム内のプロンプトの設定
case $TERM in
dumb|emacs|sun) PROMPT='%n@%m:%3~ %(!.#.$) ' ;;
*)
#if ( which sed >& /dev/null )
#then
#    name_s=`echo $USER | sed 's/\(^....\).*$/\1/'`
#    host_s=`echo $HOST | sed 's/\(^...\).*$/\1/'`
#else
    name_s=`echo $USER`
    host_s=`echo $HOST`
#fi
if [ "`hostname`" = "easter" ]
then
    PROMPT="$"
elif [[ $TERM = "cygwin" ]]
then
    PROMPT='%{'$XTITLE'[$[31+$RANDOM % 6]m%}$name_s@$host_s:%2~%(!.#.$)%{[m%} '
    RPROMPT='%{'$XTITLE'[$[31+$RANDOM % 6]m%}$%7~%{[m%} '
else
    PROMPT='%{'$XTITLE'[m%}$name_s@$host_s:%4~%(!.#.$) '
fi
;;
esac

unset XTITLE
unset ESC
unset BELL

# Some environment variables
export HISTFILE=${HOME}/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
export USER=$USERNAME
export HOSTNAME=$HOST

### bindkey
# bindkey -v             # vi key bindings
bindkey -e               # emacs key bindings

## Bindkey you may think it's usefull
bindkey ' ' magic-space  # also do history expansino on space
bindkey -s "^xs" '\C-e"\C-asu -c "'
#bindkey -s "^xd" "$(date '+-%d_%b')"
bindkey -s "^xd" "$(date '+-%Y%m%d')"
#bindkey -s "^xf" "$(date '+-%D'|sed 's|/||g')"
bindkey -s "^xp" "\$(pwd\)/"
bindkey -s "^xw" "\C-a \$(which \C-e\)\C-a"

# ctrl+j で コマンドの途中からヒストリを呼び出す
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
bindkey "^J" history-beginning-search-backward-end

if ( which dircolors >& /dev/null ); then
    if [ -f ~/.dircolors ] ; then
	eval `dircolors --sh ~/.dircolors`
    fi
    export LS_COLORS="${LS_COLORS}:*~=01;42:*#=01;42:*%=01;42"
else

# lsの色の設定
export LS_COLORS="\
*~=32;1:*#=32;1:*%=32;1:\
*README=31;4:*eadme=31;4:\
*.c=31:*.cc=31:*.cpp=31:*.C=31:*.cxx=31:*.h=31:*.o=32:*Makefile=31;43:\
*.html=31:*.htm=31:*.shtml=31:*.tex=31:*.lyx=31:*.mgp=31:*.pl=31:*.for=31:\
*.tar=01;31:*.tgz=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.rpm=01;31:*.lzh=01;31:*.zip=01;31:\
*.jpg=35:*.jpeg=35:*.gif=35:*.bmp=35:*.JPG=35:*.JPEG=35:*.GIF=35:*.BMP=35:\
*.eps=35:*.ppm=35:*.xbm=35:*.xpm=35:*.tif=35:\
*.mpg=01;37:*.mpeg=01;37:*.avi=01;37:*.MPG=01;37:*.MPEG=01;37:*.AVI=01;37:\
or=31;7"

fi

# Color completion.
export ZLS_COLORS=$LS_COLORS
zmodload -ui complist

###
# Set shell options

# 親プロセスが死んでも子プロセスが死なない
setopt nohup

# コマンドを訂正して下さる
setopt correct

# ファイル名も訂正して下さる
setopt correct_all

# 複数のタームで実行したコマンドのヒストリを共有化する
setopt share_history

# 直前と同じコマンドを入力した場合，ヒストリに入れない
setopt hist_ignore_dups

# コマンドの不要な空白を削除してヒストリに登録
setopt hist_reduce_blanks

# historyコマンドをヒストリに登録しない
setopt hist_no_store

# スペースで始まるコマンドをヒストリに登録しない
setopt hist_ignore_space

# コマンドの開始時間，経過時間をヒストリに保存
setopt extended_history  

# リダイレクション (>) で上書きしない
# >! で強制書き込み
# setopt no_clobber

# エラーの際のビープ音を鳴らさない
setopt no_beep

# 「#」 「~」 「^」を特殊文字として使用する
setopt extended_glob

# 補完候補をスッキリ表示
setopt list_packed

# cd を省略
setopt auto_cd

# 変数をディレクトリパスとして利用する
setopt auto_name_dirs

# cd コマンドで自動的にpushdする
setopt auto_pushd

# pushdの重複を防ぐ
setopt pushd_ignore_dups 

# popdでスタックの内容を表示しない
setopt pushd_silent

# 日本語のファイル名も補完リストで表示
setopt print_eight_bit

# rm で * を使う際に聞き返してこない
setopt rm_star_silent

# ファイル名中の数字を数字としてソート
setopt numeric_glob_sort

## TAB で順に補完候補を切り替える
## setopt auto_menu 
## TAB で順に補完候補を切り替えない
setopt noautomenu

setopt sh_word_split
setopt auto_param_keys
setopt no_list_ambiguous
#setopt dvorak

[[ $EMACS = t ]] && unsetopt zle

#zstyle ':completion:*:complete:ssh:*:hosts' hosts $hosts
# The following lines were added by compinstall

#zstyle ':completion:*' completer _oldlist _expand _complete _ignored _match _correct _approximate _prefix
zstyle ':completion:*' completer _oldlist  _expand _complete _ignored _match _prefix

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' match-original both
#zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z} m:{a-zA-Z}={A-Za-z}' 'm:{a-z}={A-Z} m:{a-zA-Z}={A-Za-z}' 'm:{a-z}={A-Z} m:{a-zA-Z}={A-Za-z}' 'm:{a-z}={A-Z} m:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' max-errors 1 numeric
zstyle ':completion:*' old-list _complete _approximate _correct _match _expand
zstyle ':completion:*' original true
zstyle :compinstall filename '~/.zshrc'
#zstyle ':completion:*:default' menu select=1

# 補完機能が強力になる
autoload -U compinit
compinit

# 日本語名の directory へ移動
ncd(){ builtin cd "`echo $@ | nkf -s`"}
nls(){ ls $@ | nkf}
# nxmms() { /usr/bin/xmms "`echo $@ | nkf -s`"}
# nplaympeg() { plaympeg "`echo $@ | nkf -s`"}

# export LM_LICENSE_FILE=1700@collabo4:7182@collabo4:1717@133.11.58.5:1709@karafuto

if [[ -n $SSH_CLIENT || -n $DISPLAY ]]; then
	LANG=ja_JP.utf-8 export LANG
	LC_ALL=ja_JP.utf-8 export LC_ALL
	XMODIFIERS=@im=uim export XMODIFIERS
fi
case $HOST in
    cooper)
    source /opt/xilinx/Vivado/2016.4/settings64.sh
    ;;    
esac
#.zshrc.local読み込み
[ -f ~/.zshrc.local ] && source ~/.zshrc.local

#matlab

export PATH=/opt/MATLAB/R2012a/bin:$PATH
export PATH=/opt/ibm/ILOG/CPLEX_Studio1262/cplex/bin/x86-64_linux:$PATH
#export LD_PRELOAD=/usr/lib/x86_64linux-gnu/libstdc++.so.6


#tmux
#login時にtmuxに入る
function is_exists() { type "$1" >/dev/null 2>&1; return $?; }
function is_osx() { [[ $OSTYPE == darwin* ]]; }
function is_screen_running() { [ ! -z "$STY" ]; }
function is_tmux_runnning() { [ ! -z "$TMUX" ]; }
function is_screen_or_tmux_running() { is_screen_running || is_tmux_runnning; }
function shell_has_started_interactively() { [ ! -z "$PS1" ]; }
function is_ssh_running() { [ ! -z "$SSH_CONECTION" ]; }

function tmux_automatically_attach_session()
{
    if is_screen_or_tmux_running; then
        ! is_exists 'tmux' && return 1

        if is_tmux_runnning; then
            echo "${fg_bold[red]} _____ __  __ _   ___  __ ${reset_color}"
            echo "${fg_bold[red]}|_   _|  \/  | | | \ \/ / ${reset_color}"
            echo "${fg_bold[red]}  | | | |\/| | | | |\  /  ${reset_color}"
            echo "${fg_bold[red]}  | | | |  | | |_| |/  \  ${reset_color}"
            echo "${fg_bold[red]}  |_| |_|  |_|\___//_/\_\ ${reset_color}"
        elif is_screen_running; then
            echo "This is on screen."
        fi
    else
        if shell_has_started_interactively && ! is_ssh_running; then
            if ! is_exists 'tmux'; then
                echo 'Error: tmux command not found' 2>&1
                return 1
            fi

            if tmux has-session >/dev/null 2>&1 && tmux list-sessions | grep -qE '.*]$'; then
                # detached session exists
                tmux list-sessions
                echo -n "Tmux: attach? (y/N/num) "
                read
                if [[ "$REPLY" =~ ^[Yy]$ ]] || [[ "$REPLY" == '' ]]; then
                    tmux attach-session
                    if [ $? -eq 0 ]; then
                        echo "$(tmux -V) attached session"
                        return 0
                    fi
                elif [[ "$REPLY" =~ ^[0-9]+$ ]]; then
                    tmux attach -t "$REPLY"
                    if [ $? -eq 0 ]; then
                        echo "$(tmux -V) attached session"
                        return 0
                    fi
                fi
            fi

            if is_osx && is_exists 'reattach-to-user-namespace'; then
                # on OS X force tmux's default command
                # to spawn a shell in the user's namespace
                tmux_config=$(cat $HOME/.tmux.conf <(echo 'set-option -g default-command "reattach-to-user-namespace -l $SHELL"'))
                tmux -f <(echo "$tmux_config") new-session && echo "$(tmux -V) created new session supported OS X"
            else
                tmux new-session && echo "tmux created new session"
            fi
        fi
    fi
}
tmux_automatically_attach_session
